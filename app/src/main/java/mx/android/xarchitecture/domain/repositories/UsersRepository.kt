package mx.android.xarchitecture.domain.repositories

import mx.android.xarchitecture.domain.models.User

interface UsersRepository : Repository {
    fun loadUsers(callback: Repository.OnResultCallback<List<User>>)
    fun loadUser(id: Long, callback: Repository.OnResultCallback<User?>)
}



