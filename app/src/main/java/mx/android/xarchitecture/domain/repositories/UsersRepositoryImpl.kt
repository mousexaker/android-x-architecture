package mx.android.xarchitecture.domain.repositories

import android.util.Log
import mx.android.xarchitecture.domain.database.Database
import mx.android.xarchitecture.domain.models.User
import mx.android.xarchitecture.domain.webapi.WebService

class UsersRepositoryImpl(private val db: Database, private val webService: WebService) : UsersRepository {

    init {
        Log.d("DIP", "UsersRepositoryImpl")
    }

    override fun loadUsers(callback: Repository.OnResultCallback<List<User>>) {
        val usersDao = db.getUsersDao()
        if (usersDao.isEmpty()) {
            webService.loadUsers(object : WebService.OnResultCallback<List<User>> {
                override fun onResult(result: List<User>, successful: Boolean, error: WebService.Error) {
                    usersDao.saveUsers(result)
                    val users = usersDao.getUsers()
                    callback.onResult(users, true, Repository.OnResultCallback.success())
                }
            })
        } else {
            val users = usersDao.getUsers()
            callback.onResult(users, true, Repository.OnResultCallback.success())
        }
    }

    override fun loadUser(id: Long, callback: Repository.OnResultCallback<User?>) {
        val user = db.getUsersDao().getUser(id)
        callback.onResult(user, true, Repository.OnResultCallback.success())
    }
}