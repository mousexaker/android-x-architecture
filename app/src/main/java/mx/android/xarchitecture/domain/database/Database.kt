package mx.android.xarchitecture.domain.database

interface Database {
    fun getUsersDao(): UsersDao
}