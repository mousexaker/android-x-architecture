package mx.android.xarchitecture.domain.models

data class User(val id: Long, val name: String, val age: Int)

