package mx.android.xarchitecture.domain.database

import android.content.Context
import android.util.Log

class DatabaseImpl(context: Context) : Database {

    init {
        Log.d("DIP", "DatabaseImpl")
    }

    private val usersDao = UsersDao()

    override fun getUsersDao() = usersDao


}