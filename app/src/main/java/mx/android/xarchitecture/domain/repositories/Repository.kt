package mx.android.xarchitecture.domain.repositories

interface Repository {

    interface OnResultCallback<T> {
        companion object {
            fun success() = Repository.Error(Error.Status.OK)
            fun fail(message: String?) = Repository.Error(Error.Status.OK, message)
        }

        fun onResult(result: T, successful: Boolean, error: Error)
    }

    class Error(val status: Status? = null, val sessage: String? = null) {
        enum class Status {
            OK, FAIL
        }
    }
}