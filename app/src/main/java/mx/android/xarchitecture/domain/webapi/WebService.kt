package mx.android.xarchitecture.domain.webapi

import mx.android.xarchitecture.domain.models.User

interface WebService {

    fun loadUsers(callback: OnResultCallback<List<User>>)


    interface OnResultCallback<T> {
        companion object {
            fun success() = Error(Error.Status.OK)
            fun fail(message: String?) = Error(Error.Status.OK, message)
        }

        fun onResult(result: T, successful: Boolean, error: Error)
    }

    class Error(val status: Status? = null, val sessage: String? = null) {
        enum class Status {
            OK, FAIL
        }
    }

}