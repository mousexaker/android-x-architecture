package mx.android.xarchitecture.domain.webapi

import android.content.Context
import android.util.Log
import mx.android.xarchitecture.domain.models.User
import kotlin.random.Random

class WebServiceImpl(private val context: Context) : WebService {
    override fun loadUsers(callback: WebService.OnResultCallback<List<User>>) {
        Thread {
            val result = mutableListOf<User>()
            for (i in 1..100) {
                result.add(User(i.toLong(), "User $i", getRandomAge(10, 50)))
            }
            try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
            }
            callback.onResult(result, true, WebService.OnResultCallback.success())
        }.start()
    }

    private fun getRandomAge(start: Int, stop: Int) = Random.nextInt(start, stop)

    init {
        Log.d("DIP", "WebServiceImpl")
    }


}