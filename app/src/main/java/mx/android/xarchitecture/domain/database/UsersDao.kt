package mx.android.xarchitecture.domain.database

import mx.android.xarchitecture.domain.models.User

class UsersDao {
    private val storage = mutableMapOf<Long, User>()

    fun isEmpty() = storage.isEmpty()

    fun getUsers(): List<User> = storage.values.toList()


    fun getUser(id: Long): User? {
        for (user in storage.values) {
            if (user.id == id) return user
        }
        return null
    }

    fun saveUsers(users: List<User>) {
        for (user in users) {
            storage[user.id] = user
        }
    }
}
