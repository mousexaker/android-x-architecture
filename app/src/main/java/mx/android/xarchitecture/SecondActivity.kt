package mx.android.xarchitecture

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import mx.android.xarchitecture.ui.UsersFragment

class SecondActivity : AppCompatActivity(), UsersFragment.OnFragmentInteractionListener {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
