package mx.android.xarchitecture.di

import dagger.Module
import dagger.Provides
import mx.android.xarchitecture.domain.database.Database
import mx.android.xarchitecture.domain.repositories.UsersRepository
import mx.android.xarchitecture.domain.repositories.UsersRepositoryImpl
import mx.android.xarchitecture.domain.webapi.WebService
import javax.inject.Singleton

@Module
class RepositoriesModule {

    @Provides
    @Singleton
    fun provideUsersRepository(db: Database, webService: WebService): UsersRepository =
        UsersRepositoryImpl(db, webService)
}