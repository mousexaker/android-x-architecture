package mx.android.xarchitecture.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import mx.android.xarchitecture.ui.UsersFragment


@Module
abstract class AppBinderModule {

    @ContributesAndroidInjector
    abstract fun usersFragment(): UsersFragment
}