package mx.android.xarchitecture.di

import android.content.Context
import dagger.Module
import dagger.Provides
import mx.android.xarchitecture.domain.database.Database
import mx.android.xarchitecture.domain.database.DatabaseImpl
import mx.android.xarchitecture.domain.webapi.WebService
import mx.android.xarchitecture.domain.webapi.WebServiceImpl
import javax.inject.Singleton

@Module
class PersistenceModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): Database = DatabaseImpl(context)

    @Provides
    @Singleton
    fun provideWebservice(context: Context): WebService = WebServiceImpl(context)
}