package mx.android.xarchitecture.di

import dagger.Module
import dagger.Provides
import mx.android.xarchitecture.domain.repositories.UsersRepository
import mx.android.xarchitecture.ui.UsersViewModelFactory

@Module
class ViewModelsModule {

    @Provides
    fun provideUsersViewModelFactory(repository: UsersRepository) = UsersViewModelFactory(repository)
}