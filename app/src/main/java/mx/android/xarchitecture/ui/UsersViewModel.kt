package mx.android.xarchitecture.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import mx.android.xarchitecture.domain.models.User
import mx.android.xarchitecture.domain.repositories.Repository
import mx.android.xarchitecture.domain.repositories.UsersRepository


class UsersViewModel(private val repository: UsersRepository) : ViewModel() {

    init {
        Log.d("DIP", "UsersViewModel - create")
    }

    private val users = MutableLiveData<List<User>>()
    private val user = MutableLiveData<User?>()

    fun getUsers(): LiveData<List<User>> = users
    fun getUser(): LiveData<User?> = user

    fun loadUsers() {
        repository.loadUsers(object : Repository.OnResultCallback<List<User>> {
            override fun onResult(result: List<User>, successful: Boolean, error: Repository.Error) {
                users.postValue(result)
            }
        })
    }

    fun loadUser(id: Long) {
        repository.loadUser(id, object : Repository.OnResultCallback<User?> {
            override fun onResult(result: User?, successful: Boolean, error: Repository.Error) {
                user.value = result
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("DIP", "UsersViewModel - destroy")
    }
}