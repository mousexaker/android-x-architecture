package mx.android.xarchitecture.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import mx.android.xarchitecture.domain.repositories.UsersRepository

@Suppress("UNCHECKED_CAST")
class UsersViewModelFactory(private val userRepository: UsersRepository) : ViewModelProvider.Factory {

    init {
        Log.d("DIP", "UsersViewModelFactory")
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UsersViewModel(userRepository) as T
    }
}